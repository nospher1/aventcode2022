def main():
    listLines = getFilelines("Inputs.txt")
       
    print(part1(listLines))
    print(part2(listLines))
  
   

    
def part2(listLines):
    score = 0
    for line in listLines:       
        if(line.strip()):
            splitted  = line.split(" ")
            choix1 = splitted[0]
            choix2 = splitted[1][0]
            
            
            if choix2 == "X":#loos
                score += 0
                if choix1 =="A":
                    score += 3
                if choix1 == "B":
                    score += 1
                if choix1 == "C":
                    score += 2
            if choix2 == "Y": #draw
                score += 3
                if choix1 =="A":
                    score += 1
                if choix1 == "B":
                    score += 2
                if choix1 == "C":
                    score += 3
    
            if choix2 == "Z":#win
                score += 6
                if choix1 =="A":
                    score += 2
                if choix1 == "B":
                    score += 3
                if choix1 == "C":
                    score += 1     
            
    return (score);   
   
def part1(listLines):
    
    score = 0
    for line in listLines:       
        if(line.strip()):
            splitted  = line.split(" ")
            choix1 = splitted[0]
            choix2 = splitted[1][0]
            
            
            if choix2 == "X":
                score += 1
                if choix1 =="A":
                    score += 3
                if choix1 == "B":
                    score += 0
                if choix1 == "C":
                    score += 6
            if choix2 == "Y":
                score += 2
                if choix1 =="A":
                    score += 6
                if choix1 == "B":
                    score += 3
                if choix1 == "C":
                    score += 0

            if choix2 == "Z":#win
                score += 3
                if choix1 =="A":
                    score += 0
                if choix1 == "B":
                    score += 6
                if choix1 == "C":
                    score += 3     

           
                
                
    return (score);   


def getFilelines(filename):
    with open(filename, "r") as f:
        return f.readlines()        


if __name__ == "__main__":
    main()