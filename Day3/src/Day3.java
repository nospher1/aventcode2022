import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day3 {

    private static void part1(List<String> lines, HashMap<Character, Integer> mapChar_Prio){

        int sum = 0;


        for (String line: lines) {

            final int mid = line.length() / 2;

            List<Character> commona = line.substring(0, mid).chars().mapToObj(c -> (char) c).collect(Collectors.toList());
            List<Character> commonb = line.substring(mid).chars().mapToObj(c -> (char) c).collect(Collectors.toList());
            commona.retainAll(commonb);

            sum += mapChar_Prio.get(commona.get(0));

        }

        System.out.println(sum);

    }

    private static void part2(List<String> lines, HashMap<Character, Integer> mapChar_Prio){

        int sum = 0;


        for (int i = 0; i < lines.size() ; i = i +3) {

            List<Character> commona =  lines.get(i).chars().mapToObj(c -> (char) c).collect(Collectors.toList());
            List<Character> commonb =  lines.get(i+1).chars().mapToObj(c -> (char) c).collect(Collectors.toList());
            List<Character> commonc =  lines.get(i+2).chars().mapToObj(c -> (char) c).collect(Collectors.toList());

            commonb.retainAll(commonc);
            commona.retainAll(commonb);

            sum += mapChar_Prio.get(commona.get(0));

        }

        System.out.println(sum);

    }

    public static void main(String[] args) {

        List<String> result;
        try (Stream<String> lines = Files.lines(Paths.get("E:\\Sources\\POCReverse\\src\\Utils.txt"))) {
            result = lines.collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        char[] alphabetUpper = "abcdefghijklmnopqrstuvwxyz".toUpperCase().toCharArray();

        HashMap<Character, Integer> mapChar_Prio = new HashMap<>();

        for(int i = 0; i < 26; i++){
            mapChar_Prio.put(alphabet[i], i+1 );
            mapChar_Prio.put(alphabetUpper[i], i+27);

        }

        part1(result, mapChar_Prio);
        part2(result, mapChar_Prio);

    }



}
