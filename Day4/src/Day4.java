import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Day4 {

    public static void main(String[] args) {

        List<String> result;
        try (Stream<String> lines = Files.lines(Paths.get("E:\\inputs.txt"))) {
            result = lines.collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


        int total = 0;
        int total2 = 0;

        for (String line: result) {

            String secteurA = line.split(",")[0];
            String secteurB = line.split(",")[1];

            List<Integer> listeSecteurA = IntStream.rangeClosed(Integer.parseInt(secteurA.split("-")[0]), Integer.parseInt(secteurA.split("-")[1])).boxed().toList();
            List<Integer> listeSecteurB = IntStream.rangeClosed(Integer.parseInt(secteurB.split("-")[0]), Integer.parseInt(secteurB.split("-")[1])).boxed().toList();

            if(listeSecteurB.contains(listeSecteurA.get(0)) && listeSecteurB.contains(listeSecteurA.get(listeSecteurA.size()-1)))
                total++;
            else if(listeSecteurA.contains(listeSecteurB.get(0)) && listeSecteurA.contains(listeSecteurB.get(listeSecteurB.size()-1)))
                total++;


            if(listeSecteurA.stream()
                    .distinct()
                    .filter(listeSecteurB::contains)
                    .collect(Collectors.toSet()).size() > 0){
                total2++;
            }
        }

        System.out.println(total);
        System.out.println(total2);
    }


}





