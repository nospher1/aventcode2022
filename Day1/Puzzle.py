def main():
    listLines = getFilelines("Inputs.txt")
    part1(listLines)
    part2(listLines) 



def part1(listLines):  
    bagList = generateBagList(listLines)
    print(max(bagList)) 
    
    
def part2(listLines):   
    bagList = generateBagList(listLines)
    bagList.sort()
    
    print(sum(bagList[-3:]))
    
    
def generateBagList(listLines):
    bagList = []
    bag = 0
    
    for line in listLines:       
        if(line.strip()):
            bag = bag +  int(line)
        else:
            bagList.append(bag)
            bag = 0
           
    return bagList


def getFilelines(filename):
    with open(filename, "r") as f:
        return f.readlines()        


if __name__ == "__main__":
    main()